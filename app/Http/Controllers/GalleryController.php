<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;

class GalleryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->only(['create','edit']);
    }

    /**
     * Resource For Gallery Index
     */

    public function index()
    {
        $galleries = Gallery::get();
        return view('galleries.index',compact('galleries'));
    }

    public function create()
    {
        return view('galleries.create');
    }

    public function store(Request $request)
    {
        $this->validate(request(),[
            'title' => 'required',
            'description' => 'required',
        ]);

        $gallery = new Gallery;

        $gallery->title = $request->title;
        $gallery->description = $request->description;
        if($request->hasFile('image')){
            $request->file('image')->store('public/gallery/');
            $gallery->image = $request->file('image')->hashName('gallery/');
        }
        $gallery->save();
        return redirect('/@');

    }
}
