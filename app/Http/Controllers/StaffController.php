<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Staff;

class StaffController extends Controller
{
    public function __construct()
    {  
        $this->middleware('auth')->only(['create','edit']);
    }

    public function index()
    {
        $staff = Staff::get();
        return view('staff.index',compact('staff'));
    }

    public function create()
    {
        return view('staff.create');
    }

    public function store(Request $request)
    {
        $data = $this->validate(request(),[
            'name' => 'required',
            'position' => 'required',
            'image' => 'required'
        ]);
        $staff = new Staff;
        $staff->fill($data);
        if($request->hasFile('image')){
            $request->file('image')->store('public/staff/');
            $staff->image = $request->file('image')->hashName('staff/');
        }
        $staff->save();
        return redirect('/company');
    }
}
