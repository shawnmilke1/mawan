@extends('layouts.app')
@section('content')
    <section class="ui-section ui-bg-light-blue">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h2 class="display-4">
                        At AMA, we're Improving lives through planes
                    </h2>
                </div>
            </div>
        </div>
    </section>
    <section class="ui-section">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-7">
                    <h2 class="section-title text-center">
                        Caption One
                    </h2>
                    <p class="text-justify">
                        PT. AIRMAS MULYA ABADI is a private company engaged in the provision of goods and services. The company is based in BOGOR is active in the procurement of spare parts plane type Fix Wing and Rotary. Category Fix Wing This company provides spare parts for Cessena, Cassa and Bombardier aircraft type, while for Rotary category include Bell, Bolcow, MI and other aircraft.
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection