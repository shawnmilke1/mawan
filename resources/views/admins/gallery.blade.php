<div class="row">
    <div class="col-md">
        <h4>Libraries</h4>
    </div>
    <div class="col-md text-right">
        <a href="{{url('gallery/create')}}" class="btn btn-primary">Add New Photo</a>
    </div>
</div>
<hr>
<div class="row">
    @foreach ($galleries as $gallery)
        <div class="col-md-6">
            <div class="card">
                <img src="{{asset('storage/'.$gallery->image)}}" alt="" class="card-img-top">
            </div>
        </div>
    @endforeach
</div>