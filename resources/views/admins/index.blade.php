@extends('layouts.app')

@section('content')
<section class="ui-section ui-bg-light">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-center">
                            {{Auth::user()->name}}
                        </h4>
                        <div class="text-center mt-5">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>Total Galleries</h5>
                                    <span class="display-4">{{$galleries->count()}}</span>
                                </div>
                                <div class="col-md-6">
                                    <h5 class="">Total Company</h5>
                                    <span class="display-4">{{$staff->count()}}</span>
                                </div>
                            </div>
                            <div class="mt-5">
                                <ul class="list-group">
                                    <a href="#" class="list-group-item list-group-item-action {{Request::is('@') ? 'active' : '' }}">Gallery</a>
                                    <a href="#" class="list-group-item list-group-item-action">Company</a>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 mt-2">
                @include('admins.gallery')
            </div>
        </div>
    </div>
</section>
@endsection
