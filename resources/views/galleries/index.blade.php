@extends('layouts.app')
@section('title','Gallery')
@section('content')
<section class="ui-section">
    <div class="container">
        <div class="text-center">
            <h2 class="font-weight-bold">Hello!</h2>
            <p>
                This is our Photo Gallery, Enjoy!.
            </p>
        </div>
        <div class="row">
            @foreach ($galleries as $gallery)
                <div class="col-md-4">
                    <img src="{{asset('storage/'.$gallery->image)}}" class="img-thumbnail" alt="{{$gallery->title}}" >
                </div>
            @endforeach
        </div>
    </div>
</section>
@endsection