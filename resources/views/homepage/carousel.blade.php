<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
        <img class="d-block w-100" src="{{asset('assets/cover/carousel1.jpg')}}" alt="First slide">
        <div class="carousel-caption ">
            <p>PT. AMA is a company engaged in the service of Aircraft spare part, component is committed to always provide the best service to customers.</p>
        </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{asset('assets/cover/carousel2.jpg')}}" alt="Second slide">
      <div class="carousel-caption ">
        <p>
          Provides spartpart and component as well as equipment for repair.
        </p>
      </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{asset('assets/cover/carousel3.jpg')}}" alt="Third slide">
      <div class="carousel-caption">
        <p>
          Builds cooperation to provide repair tools
        </p>
      </div>
    </div>
  </div>
</div>