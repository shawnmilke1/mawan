@extends('layouts.app')
@section('content')
    <section class="welcome-section-1 ui-bg-dark-blue">
        @include('homepage.carousel')
    </section>
    <section class="ui-section-small ui-bg-light border-bottom-light">
        <div class="container">
            <h4 class="display-5 text-center text-muted">Use More Than 100,000 Organiations Around the world</h4>
        </div>
    </section>
    <section class="ui-section-large border-bottom-light">
        <div class="container text-center">
            <div class="row justify-content-md-center">
                <div class="col-md-8">
                    <span class="text-uppercase text-muted">Hello and Welcome to PT Airmas Mulya Abadi</span>
                    <h4 class="mt-3 font-weight-light">
                        PT. AIRMAS MULYA ABADI is a private company engaged in the provision of goods and services. The company is based in BOGOR is active in the procurement of spare parts plane type Fix Wing and Rotary. Category Fix Wing This company provides spare parts for Cessena, Cassa and Bombardier aircraft type, while for Rotary category include Bell, Bolcow, MI and other aircraft.
                    </h4>
                </div>
            </div>
        </div>
    </section>
    <section class="ui-section border-bottom-light">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md text-center">
                    <img src="https://i.imgur.com/aUgUeO4.png" width="300" height="auto">
                </div>
                <div class="col-md">
                    <h3 class="display-4">AMA Is Awesome</h3>
                    <p class="text-muted">
                        AMA can save you much time and it will make building applications even more fun!
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="ui-section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md">
                    <h3 class="display-5">Functionality is Baked Right In!</h3>
                    <p>
                        Baked right in like a fresh loaf of BREAD! Voyager's admin interface allows you to create CRUD or BREAD (Browse, Read, Edit, Add, and Delete) functionality to your posts, pages, or any other table in your database.
                    </p>
                </div>
                <div class="col-md text-center">
                    <img src="https://i.imgur.com/aUgUeO4.png" width="300" height="auto">
                </div>
            </div>
        </div>
    </section>
@endsection