<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Airmas Mulya Abadi - @yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-ama sticky-top">
            <div class="container">
                <a class="navbar-brand text-bold" href="{{ url('/') }}">
                    <img src="{{asset('assets/logo/logo1.png')}}" width="50" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto font-weight-bold">
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('about')}}">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('company.index')}}">Company</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('gallery.index')}}">Gallery</a>
                        </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    @auth
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a href="/@" class="nav-link">
                                Admin Dashboard
                            </a>
                        </li>
                    </ul>
                    @endauth
                </div>
            </div>
        </nav>

        <main>
            @yield('content')
        </main>
        @include('layouts.footer')
    </div>
</body>
</html>
