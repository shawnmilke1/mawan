<footer class="ui-footer text-white">
    <div class="container">
        <div class="row">
            <div class="col-md text-center">
                <h4 class="footer-title">
                    Address
                </h4>
                <p class="footer-subtitle" >Alamat Perusahaan</p>
            </div>
            <div class="col-md">
                <div class="text-center">
                    <h4 class="footer-title">
                        Stay Tuned
                    </h4>
                    <p class="footer-subtitle">
                        Connect with us and stay in the loop
                    </p>
                </div>
                <ul class="nav social-media justify-content-center">
                    <li class="nav-item">
                        <a href="">
                            <i class="fab fa-facebook-f fa-2x"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="">
                            <i class="fab fa-twitter fa-2x"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md text-center">
                <h4 class="footer-title">
                    Contact Us
                </h4>
                <p class="footer-subtitle">
                    Have A Question? Try Us!.
                </p>
            </div>
        </div>
    </div>
</footer>
<div class="p-3 copyright">
    <div class="container text-center">
        <span class="font-weight-bold">Copyright</span>
    </div>
</div>