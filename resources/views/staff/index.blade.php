@extends('layouts.app')
@section('title','Company')
@section('content')
    <section class="ui-section ui-bg-light text-center">
        <div class="container">
            <h2>Meet Our Company Team</h2>
            <p>Our Company Team share for building successful companies</p>
        </div>
    </section>
    <section class="ui-section">
        <div class="container">
            <div class="row">
                @foreach ($staff as $person)
                    <div class="col-md-4">
                        <div class="card">
                            <img src="{{asset('storage/'.$person->image)}}" class="card-img-top" alt="{{$person->name}}">
                            <div class="card-body">
                                <h5 class="card-title">{{$person->name}}</h5>
                                <span class="card-text">{{$person->position}}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection