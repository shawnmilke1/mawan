<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/','homepage.index')->name('index');
Route::view('/about','about');
Route::get('/ama-login','Auth\LoginController@showLoginForm')->name('login');
Route::post('/ama-login','Auth\LoginController@login')->name('login');
// Auth::routes(); use later

Route::get('/@', 'HomeController@index')->name('home');
//gallery
Route::get('/gallery','GalleryController@index')->name('gallery.index');
Route::get('/gallery/create','GalleryController@create');
Route::post('/gallery/create','GalleryController@store');

//Staff
Route::get('/company','StaffController@index')->name('company.index');
Route::get('/company/create','StaffController@create')->name('company.create');
Route::post('/company','StaffController@store')->name('company.store');